msgid ""
msgstr ""
"Project-Id-Version: anthra_1920_OS\n"
"Report-Msgid-Bugs-To: </var/mail/root>\n"
"POT-Creation-Date: 2010-10-24 05:12+0200\n"
"PO-Revision-Date: \n"
"Last-Translator: Tomas Saxer <tsaxer@gmx.de>\n"
"Language-Team:  <tsaxer@gmx.de>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

msgid "Setup"
msgstr "Einstellungen"

msgid "Timers"
msgstr "Timer"

msgid "Commands"
msgstr "Befehle"

msgid "Recordings"
msgstr "Aufzeichnungen"

msgid "Schedule"
msgstr "Programm"

msgid "Channels"
msgstr "Kanäle"
