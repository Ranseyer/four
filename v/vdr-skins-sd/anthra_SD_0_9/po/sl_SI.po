# VDR language source file.
# Copyright (C) 2008 Klaus Schmidinger <kls@tvdr.de>
# This file is distributed under the same license as the VDR package.
# Miha Setina <mihasetina@softhome.net>, 2000
# Matjaz Thaler <matjaz.thaler@guest.arnes.si>, 2003, 2005, 2006, 2008
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.6.0\n"
"Report-Msgid-Bugs-To: </var/mail/root>\n"
"POT-Creation-Date: 2010-10-24 14:43+0200\n"
"PO-Revision-Date: 2010-10-16 09:28+0100\n"
"Last-Translator: Tomas Saxer <tsaxer@gmx.de>\n"
"Language-Team: Slovenian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

msgid "Volume"
msgstr "Glasnost "

msgid "Mute"
msgstr "Izklop zvoka"

#~ msgid "Setup"
#~ msgstr "Nastavitve"

#~ msgid "Timers"
#~ msgstr "Termini"

#~ msgid "Commands"
#~ msgstr "Ukazi"

#~ msgid "Recordings"
#~ msgstr "Posnetki"

#~ msgid "Schedule"
#~ msgstr "Program"

#~ msgid "Channels"
#~ msgstr "Kanali"
