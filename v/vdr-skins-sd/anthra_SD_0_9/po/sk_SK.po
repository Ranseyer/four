# VDR language source file.
# Copyright (C) 2008 Klaus Schmidinger <kls@tvdr.de>
# This file is distributed under the same license as the VDR package.
# Vladim�r B�rta <vladimir.barta@k2atmitec.cz>, 2006, 2008
# Ji�� Dobr� <jdobry@centrum.cz>, 2008
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.6.0\n"
"Report-Msgid-Bugs-To: </var/mail/root>\n"
"POT-Creation-Date: 2010-10-24 14:43+0200\n"
"PO-Revision-Date: 2010-10-16 09:27+0100\n"
"Last-Translator: Tomas Saxer <tsaxer@gmx.de>\n"
"Language-Team: Slovak\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

msgid "Volume"
msgstr "Hlasitos� "

msgid "Mute"
msgstr "Ticho"

#~ msgid "Setup"
#~ msgstr "Nastavenie"

#~ msgid "Timers"
#~ msgstr "Pl�ny nahr�vania"

#~ msgid "Commands"
#~ msgstr "Pr�kazy"

#~ msgid "Recordings"
#~ msgstr "Z�znamy"

#~ msgid "Schedule"
#~ msgstr "TV program"

#~ msgid "Channels"
#~ msgstr "Kan�ly"
