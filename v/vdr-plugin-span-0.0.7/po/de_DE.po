# VDR plugin language source file.
# Copyright (C) 2007 Christian Leuschen <christian.leuschen@gmx.de>
# This file is distributed under the same license as the VDR package.
# Klaus Schmidinger <kls@cadsoft.de>, 2000
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.5.9\n"
"Report-Msgid-Bugs-To: christian.leuschen@gmx.de\n"
"POT-Creation-Date: 2007-09-28 13:24+0200\n"
"PO-Revision-Date: 2008-08-13 13:24+0200\n"
"Last-Translator: Christian Leuschen <christian.leuschen@gmx.de>\n"
"Language-Team: <vdr@linuxtv.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

#: menu.c:33
msgid "Menu.Span$Span: Service providers"
msgstr "Span: Diensteanbieter"

#: menu.c:45
msgid "Menu.Span$Providers:"
msgstr "Datenquellen:"

#: menu.c:59 menu.c:78
msgid "activated"
msgstr "aktiviert"

#: menu.c:59 menu.c:78
msgid "deactivated"
msgstr "deaktiviert"

#: menu.c:59 menu.c:78
msgid "idle"
msgstr "unt�tig"

#: menu.c:59
msgid "providing"
msgstr "liefert"

#: menu.c:65
msgid "Menu.Span$Visualizations:"
msgstr "Visualisierungen:"

#: menu.c:78
msgid "visualizing"
msgstr "stellt dar"

#: setup.c:59
msgid "Setup.Span$Activate spectrum analyzer?"
msgstr "Aktiviere den Spectrum Analyzer"

#: setup.c:59 setup.c:61 setup.c:62 setup.c:67
msgid "no"
msgstr "nein"

#: setup.c:59 setup.c:61 setup.c:62 setup.c:67
msgid "yes"
msgstr "ja"

#: setup.c:60
msgid "Setup.Span$Visualization delay (in ms)"
msgstr "Verz�gere Visualisierung (in ms)"

#: setup.c:61
msgid "Setup.Span$Use logarithmic diagram"
msgstr "Logarithmische Darstellung"

#: setup.c:62
msgid "Hide mainmenu entry"
msgstr "Hauptmen�eintrag verstecken"

#: setup.c:67
msgid "Setup.Span$Use pure (unequalized) data"
msgstr "Pure Daten (ohne Entzerrung) visualisieren"

#: span.c:40
msgid "Spectrum Analyzer for music-data"
msgstr "Spectrum Analyzer f�r Musikdaten"

#: span.c:41
msgid "Spectrum Analyzer"
msgstr "Spectrum Analyzer"
