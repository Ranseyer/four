#!/bin/bash
# $Id: vdrshutdown 773 2004-08-23 01:22:35Z bistr-o-math $

NVRAMCMD=/usr/local/bin/nvram-wakeup
SVDRPCMD=/usr/local/src/VDR/svdrpsend.pl
LOGGER=/bin/logger
CHECKSCRIPT=/path/to/the/script

#################
##  if the script $CHECKSCRIPT thinks that we should     shutdown,
##     it must not print anything on stdout.
##  if it                      thinks that we should NOT shutdown,
##     it must print a one-line message on stdout describing the reason.
##
##  note that all parameters passed to vdrshutdown will also be passed to $CHECKSCRIPT
##  so it might use them (e.g. $5 -eq 0 below)
##
##  it might look like this:
##  --------------------------
##    #!/bin/bash
##  
##    pgrep 2divx >/dev/null 2>&1       && { echo Divx-Conversion is still running;  exit; }
##
##    test -a /some/file                && { echo /some/file exists;                 exit; }
##
##    USERCOUNT=`who | wc -l`;
##    test $5 -eq 0 -a $USERCOUNT -gt 0 && { echo "$USERCOUNT users are logged in."; exit; }
##
##  --------------------------
##
##  now if $CHECKSCRIPT exists and is executable, the message will be passed through
##  SVDRP to the tv screen.
##

test -x $CHECKSCRIPT && {
     msg=`$CHECKSCRIPT "$@"` 
     test "$msg" != "" && {
          $SVDRPCMD MESG $msg &
          $LOGGER -t `basename $0` "shutdown aborted: $msg"
          exit 1
     }
}

#################

# Add here needed options like --configfile=...
# (read 'man nvram-wakeup' and 'man nvram-wakeup.conf' for more details)
$NVRAMCMD --syslog --settime $1 

# if you are going to use the set_timer script instead of nvram-wakeup,
# comment out the line above and uncomment the following line.
# (read the comments inside the script for more details)
# $PATH_TO_SET_TIMER/set_timer $1 $2


case $PIPESTATUS in
     0) # all went ok - new date and time set
        shutdown -h now
        EXITSTATUS=0
        ;;
     1) # all went ok - new date and time set.
        #
        # *** but we need to reboot. ***
        #
        # for some boards this is needed after every change.
        #
        # for some other boards, we only need this after changing the
        # status flag, i.e. from enabled to disabled or the other way.
        
        
        # For plan A - (Plan A is not supported anymore---see README)
        #
        # For plan B - (don't forget to install the modified kernel image first)
        #
        
        lilo -R PowerOff
        
        shutdown -r now
        EXITSTATUS=0
        ;;
     2) # something went wrong
        # don't do anything - just exit with status 1
        EXITSTATUS=1
        ;;
esac

# exit with 0 if everything went ok.
# exit with 1 if something went wrong.
exit $EXITSTATUS
