/*
 *   NVRAM WakeUp
 *   Copyright (C) 2001-2003, Sergei Haller.
 *
 *   $Id: rtc.c 381 2003-03-18 13:44:07Z bistr-o-math $
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *   This file is taken (with small modifications) from:
 *
 *	Real Time Clock Driver Test/Example Program
 *
 *	Compile with:
 *		gcc -s -Wall -Wstrict-prototypes rtc.c -o rtc
 *
 *	Copyright (C) 1996, Paul Gortmaker.
 *
 *	Released under the GNU General Public License, version 2,
 *	included herein by reference.
 *
 */

#define CVSREV_rtc_c "$Id: rtc.c 381 2003-03-18 13:44:07Z bistr-o-math $"

#include <stdio.h>
#include <linux/rtc.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>

int main(void) {

     int             fd, retval;
     struct rtc_time rtc_tm;

     fd = open ("/dev/rtc", O_RDONLY);

     if (fd ==  -1) {
     	perror("/dev/rtc");
     	exit(errno);
     }

     /* Read the current alarm settings */
     retval = ioctl(fd, RTC_ALM_READ, &rtc_tm);
     if (retval == -1) {
     	perror("ioctl");
     	exit(errno);
     }

     fprintf(stderr, "Alarm time now set to %02d:%02d:%02d.\n",
     	rtc_tm.tm_hour, rtc_tm.tm_min, rtc_tm.tm_sec);

     close(fd);
     return 0;

} /* end main */
