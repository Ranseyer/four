/*
 *   NVRAM WakeUp
 *   Copyright (C) 2001-2004, Sergei Haller.
 *
 *   $Id: cat_nvram.c 682 2004-05-03 22:40:13Z bistr-o-math $
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/io.h>
#include <string.h>
#include <syslog.h>

#include "nvram-wakeup.h"

int main(int argc, char **argv)
{
     unsigned char bytes[MAXNVRAMSIZE];
     int upper_method=0;
     int size;

     /* learn who we really are */
     set_progname(argv[0]);
     enable_debug();

     if      (argc<2)                         {upper_method = 0        ;}
     else if (!strcmp(argv[1], "INTEL"     )) {upper_method = INTEL    ;}
     else if (!strcmp(argv[1], "DS1685"    )) {upper_method = DS1685   ;}
     else if (!strcmp(argv[1], "VT82Cxxx"  )) {upper_method = VT82Cxxx ;}
     else if (!strcmp(argv[1], "VT8235_37" )) {upper_method = VT8235_37;}

     memset(bytes, 0, sizeof(bytes));
     open_nvram(ON, O_RDONLY);
     size = read_whole_nvram(ON, upper_method, bytes);
     close_nvram(ON);

     /* print a nice xxd-like table of nvram */
     /* xxd(bytes, size, LOG_DEBUG);         */
     
     /* pass all bytes to stdout */
     cat(bytes, size);

	return(0);
}
