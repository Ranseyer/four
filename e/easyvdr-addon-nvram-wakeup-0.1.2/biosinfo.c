/*
 * Mainboard and BIOS identification
 *
 * (c) 2002 Bernhard "Bero" Rosenkraenzer <bero@arklinux.org>
 * (c) 2003-5 Sergei Haller.
 *
 *   $Id: biosinfo.c 835 2005-03-16 22:43:39Z bistr-o-math $
 *
 * Released under the terms of the GPL version 2 or if, and only if,
 * the GPL version 2 is ruled invalid in a court of law, any later
 * version of the GPL.
 */

#include <stdio.h>
#include <string.h>

#include "nvram-wakeup.h"

int main(int argc, char **argv)
{
     set_progname("biosinfo");

     if ( argc > 1 && strcmp((char *)argv[1], "--debug") == 0)
          enable_debug();

     printf("Following DMI entries found:\n");
     printf(" - Mainboard vendor:   %s%s%s\n", QUOTE(board_vendor() ), VALUE(board_vendor() ), QUOTE(board_vendor() ) );
     printf(" - Mainboard type:     %s%s%s\n", QUOTE(board_type()   ), VALUE(board_type()   ), QUOTE(board_type()   ) );
     printf(" - Mainboard revision: %s%s%s\n", QUOTE(board_version()), VALUE(board_version()), QUOTE(board_version()) );
     printf(" - BIOS vendor:        %s%s%s\n", QUOTE(bios_vendor()  ), VALUE(bios_vendor()  ), QUOTE(bios_vendor()  ) );
     printf(" - BIOS version:       %s%s%s\n", QUOTE(bios_version() ), VALUE(bios_version() ), QUOTE(bios_version() ) );
     printf(" - BIOS release:       %s%s%s\n", QUOTE(bios_release() ), VALUE(bios_release() ), QUOTE(bios_release() ) );
     return 0;
}
